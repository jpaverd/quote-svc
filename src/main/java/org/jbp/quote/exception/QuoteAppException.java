package org.jbp.quote.exception;

import org.jbp.quote.model.QuoteAppError;

public class QuoteAppException extends RuntimeException {
	private static final long serialVersionUID = 1L;
   private QuoteAppError error;
   
   public QuoteAppException(String message) {
      super(message);
      this.error = new QuoteAppError(message);
   }

	public QuoteAppException(QuoteAppError error) {
		super(error.getMessage());
		this.error = error;
	}

	public QuoteAppError getError() {
	   return this.error;
	}
}
