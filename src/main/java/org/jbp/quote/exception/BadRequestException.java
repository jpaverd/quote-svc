package org.jbp.quote.exception;

import org.jbp.quote.model.QuoteAppError;

public class BadRequestException extends QuoteAppException {
   private static final long serialVersionUID = 1L;

   public BadRequestException(QuoteAppError error) {
      super(error);
   }

}
