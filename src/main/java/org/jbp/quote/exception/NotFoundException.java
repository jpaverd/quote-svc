package org.jbp.quote.exception;

public class NotFoundException extends QuoteAppException {
	private static final long serialVersionUID = 1L;

	public NotFoundException(String entity, String entityId) {
	   super("There is no " + entity + " with id '" + entityId + "'");
	}

}
