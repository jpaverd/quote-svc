package org.jbp.quote.exception;

import org.jbp.quote.model.QuoteAppError;

public class DuplicateException extends QuoteAppException {
   private static final long serialVersionUID = 1L;

   public DuplicateException(QuoteAppError error) {
      super(error);
   }

}
