package org.jbp.quote.rest.controller;

import java.util.List;

import org.jbp.quote.model.Author;
import org.jbp.quote.model.AuthorSearchParms;
import org.jbp.quote.model.LimitParms;
import org.jbp.quote.model.SearchMode;
import org.jbp.quote.svc.AuthorSvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/authors")
public class AuthorRestController {

   @Autowired
   private AuthorSvc authorSvc;

   @RequestMapping(value = "/{authorId}", method = RequestMethod.GET)
   public Author getAuthor(@PathVariable int authorId) {
      return this.authorSvc.findById(authorId);
   }

   @RequestMapping(value = "/{authorId}", method = RequestMethod.DELETE)
   @ResponseStatus(HttpStatus.NO_CONTENT)
   public void deleteAuthor(@PathVariable int authorId) {
      this.authorSvc.delete(authorId);
   }

   @RequestMapping(value = "/{authorId}", method = RequestMethod.PUT)
   @ResponseStatus(HttpStatus.NO_CONTENT)
   public void updateAuthor(@PathVariable int authorId, @RequestBody Author author) {
      author.setAuthorId(authorId);
      this.authorSvc.update(author);
   }

   @RequestMapping(value = "", method = RequestMethod.POST)
   public ResponseEntity<?> createAuthor(@RequestBody Author author) {
      author.setAuthorId(null);
      int authorId = this.authorSvc.create(author);

      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.setLocation(ServletUriComponentsBuilder
               .fromCurrentRequest().path("/{id}")
               .buildAndExpand(authorId).toUri());
      return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
   }

   @RequestMapping(value = "", method = RequestMethod.GET)
   public List<Author> getAuthors(
            @RequestParam(value = "q", required = false) String q,
            @RequestParam(value = "mode", required = false) SearchMode mode,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset) {


      LimitParms limitParms = new LimitParms();
      if (limit != null)
         limitParms.setLimit(limit);
      if (offset != null)
         limitParms.setOffset(offset);

      AuthorSearchParms parms = new AuthorSearchParms(q, mode, limitParms);

      return this.authorSvc.find(parms);
   }

}
