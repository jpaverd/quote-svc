package org.jbp.quote.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.jbp.quote.exception.BadRequestException;
import org.jbp.quote.exception.DuplicateException;
import org.jbp.quote.exception.NotFoundException;
import org.jbp.quote.model.QuoteAppError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;


/**
 * @see "https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc"
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {

   private static final Logger LOG = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

   @ExceptionHandler(NotFoundException.class)
   @ResponseStatus(HttpStatus.NOT_FOUND)
   @ResponseBody
   public QuoteAppError handleNotFound(NotFoundException excep) {
      return new QuoteAppError(excep.getMessage());
   }

   @ExceptionHandler(BadRequestException.class)
   @ResponseStatus(HttpStatus.BAD_REQUEST)
   @ResponseBody
   public QuoteAppError handleBadRequest(BadRequestException ex) {
      return ex.getError();
   }

   @ExceptionHandler(DuplicateException.class)
   @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
   @ResponseBody
   public QuoteAppError handleDuplicate(DuplicateException ex) {
      return ex.getError();
   }

   @ExceptionHandler(MethodArgumentTypeMismatchException.class)
   @ResponseStatus(HttpStatus.BAD_REQUEST)
   @ResponseBody
   public QuoteAppError handleException(MethodArgumentTypeMismatchException excep) throws Exception {
 
      // LOG.error("arg excep", excep);
      List<String> details = new ArrayList<>();
      details.add(excep.getParameter().getParameterName());
      QuoteAppError error = new QuoteAppError("Invalid Request Parameter", details);
      return error;
   }

   @ExceptionHandler(Exception.class)
   @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
   @ResponseBody
   public QuoteAppError handleException(Exception excep) throws Exception {
      LOG.error(excep.getMessage(), excep); // don't expose internal details
      // If the exception is annotated with @ResponseStatus rethrow it and let
      // the framework handle it 
      if (AnnotationUtils.findAnnotation(excep.getClass(), ResponseStatus.class) != null)
         throw excep;

      return new QuoteAppError("Internal Server Error");
   }

}
