package org.jbp.quote.svc;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.jbp.quote.model.Author;
import org.jbp.quote.model.AuthorSearchParms;
import org.jbp.quote.model.SearchMode;

public interface AuthorSvc {

	int create(Author author);
	void delete(int authorId);
	void update(Author author);

	Author findById(int authorId);
	/**
	 * @param name - list of words representing a name. Must contain at least one word. Punctuation other than ' and - will be removed before searching
	 * @param mode defaults to MATCH_ANY if not provided
	 * @return list of authors matching the passed name using the search mode specified
	 * @see SearchMode
	 */
   List<Author> find(@NonNull AuthorSearchParms parms);

}
