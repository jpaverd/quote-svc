package org.jbp.quote.svc;

/**
 * Constants for service layer
 * 
 * @author johnp
 *
 */
public final class Defaults {
   private Defaults() {}

   public static final int DEFAULT_OFFSET = 0;
   public static final int DEFAULT_LIMIT = 10;
   public static final int MAX_LIMIT = 50;
}
