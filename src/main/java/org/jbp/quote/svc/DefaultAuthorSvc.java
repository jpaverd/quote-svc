package org.jbp.quote.svc;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.annotation.NonNull;
import org.jbp.quote.data.AuthorDao;
import org.jbp.quote.exception.BadRequestException;
import org.jbp.quote.exception.DuplicateException;
import org.jbp.quote.model.Author;
import org.jbp.quote.model.AuthorSearchParms;
import org.jbp.quote.model.QuoteAppError;
import org.jbp.quote.model.SearchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DefaultAuthorSvc implements AuthorSvc {

   @Autowired
   private AuthorDao authorDao;

   @Override
   public void update(Author author) {
      this.validateAuthor(author);
      this.authorDao.update(author);
   }

   private void validateAuthor(Author author) {
      ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
      Validator validator = validatorFactory.getValidator();
      Set<ConstraintViolation<Author>> violations = validator.validate(author);

      if(!violations.isEmpty()){
         List<String> errors = new ArrayList<>();
         violations.forEach(v -> errors.add(v.getPropertyPath()+" "+v.getMessage()));
         QuoteAppError qaError = new QuoteAppError("Author failed validation checks", errors);
         throw new BadRequestException(qaError);
      } 
      
      // validate that another author doesn't exist with same name
      List<Author> authors = this.authorDao.checkForDup(author);
      if (authors.size() > 0) {
         throw new DuplicateException(new QuoteAppError("Author already exists (authorId='" + authors.get(0).getAuthorId() +"')"));
      }
   }
   
   @Override
   public int create(Author author) {
      this.validateAuthor(author);
      return this.authorDao.create(author);
   }

   @Override
   public void delete(int authorId) {
      this.authorDao.delete(authorId);

   }

   @Override
   @Transactional(readOnly = true)
   public Author findById(int authorId) {
      return this.authorDao.findById(authorId);
   }

   @Override
   @Transactional(readOnly = true)
   public List<Author> find(@NonNull AuthorSearchParms parms) {

      if (StringUtils.isNotBlank(parms.getSearchString())) {
         // if search string provided, but not search mode, default mode to MATCH_ALL
         if (parms.getMode() == null) {
            parms.setMode(SearchMode.MATCH_ALL);
         }

      } else {
         parms.setSearchString(null);

         // mode without search string doesn't make sense
         if (parms.getMode() != null) {
            QuoteAppError error = new QuoteAppError("mode='" + parms.getMode()
                     + "'. If mode is specified, search string must be provided");
            throw new BadRequestException(error);
         }
      }

      if (parms.getSearchString() != null) {
         return this.authorDao.findByName(parms);
      }
      return this.authorDao.findAll(parms);
   }
}
