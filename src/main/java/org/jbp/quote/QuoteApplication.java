package org.jbp.quote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication // equivalent to using @Configuration, @EnableAutoConfiguration and @ComponentScan with their default attributes
public class QuoteApplication { // implements CommandLineRunner {
	
    public static void main(String[] args) {
        SpringApplication.run(QuoteApplication.class, args);
    }

}
