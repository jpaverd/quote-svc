package org.jbp.quote.model;

public enum SearchMode {
	/**
	 * all the words in the search criteria must match
	 */
	MATCH_ALL, 
	/**
	 * at least one word in the search criteria must match
	 */
	MATCH_ANY, 
	/**
	 * 
	 */
	EXACT_MATCH, 
	/**
	 * words in criteria are prefixes that match the start of words in the target
	 */
	WILDCARD;
}
