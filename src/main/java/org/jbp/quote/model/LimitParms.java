package org.jbp.quote.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jbp.quote.svc.Defaults;

public class LimitParms {

   private int offset = Defaults.DEFAULT_OFFSET;
   private int limit = Defaults.DEFAULT_LIMIT;
   
   public LimitParms() {
   }
   
   public int getOffset() {
      return this.offset;
   }

   public void setOffset(int offset) {
      if (offset >= 0) {
         this.offset = offset;
      } else {
         this.offset = Defaults.DEFAULT_OFFSET; 
      }
   }

   public int getLimit() {
      return this.limit;
   }

   public void setLimit(int limit) {
      if (limit <= 0) {
         this.limit = Defaults.DEFAULT_LIMIT;
      } else if (limit > Defaults.MAX_LIMIT) {
         this.limit = Defaults.MAX_LIMIT; 
      } else {
         this.limit = limit;
      }
   }

   @Override
   public String toString() {
      // return String.format("Author[authorId=%d, displayName='%s']", authorId, displayName);
      return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);

   }
   
}
