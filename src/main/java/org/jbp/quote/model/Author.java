package org.jbp.quote.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Author {

   @Min(1)
	private Integer authorId;
	
	@NotNull
	@Size(min=2,max=127)
	private String displayName;

	public Author() {
		// Default ctor needed for BeanPropertyRowMapper
	}
	
   public Author(String displayName) {
      this.displayName = displayName;
   }

	public Author(Integer authorId, String displayName) {
		this.authorId = authorId;
		this.displayName = displayName;
	}

	public Integer getAuthorId() {
		return this.authorId;
	}

	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	// can use @JsonIgnore on getter to prevent jackson from deserializing or @JsonIgnoreProperties( { "applications" })
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
