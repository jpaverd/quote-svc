package org.jbp.quote.model;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class QuoteAppError {
	
	private String message;
	private ZonedDateTime time = ZonedDateTime.now();	
	private ArrayList<String> details = new ArrayList<>();
	
	public QuoteAppError(String message) {
		this.message = message;
	}

	/**
	 * @param message human-readable message
	 * @param details more information about the error. e.g. fields that are invalid or missing
	 */
	public QuoteAppError(String message, List<String> details) {
		this(message);
		this.details = new ArrayList<>(details);
	}
	
	public String getMessage() {
		return this.message;
	}

	public ArrayList<String> getDetails() {
		return this.details;
	}

	public ZonedDateTime getTime() {
		return this.time;
	}
}
