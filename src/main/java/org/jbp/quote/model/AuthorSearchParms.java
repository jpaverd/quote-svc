package org.jbp.quote.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AuthorSearchParms {

   private String searchString;
   private SearchMode mode;
   private LimitParms limitParms;
   
   public AuthorSearchParms() {
      this.limitParms = new LimitParms();
   }
   
   public AuthorSearchParms(String searchString, SearchMode mode) {
      this();
      this.searchString = searchString;
      this.mode = mode;
   }
   
   public AuthorSearchParms(String searchString, SearchMode mode, LimitParms limitParms) {
      this.searchString = searchString;
      this.mode = mode;
      this.limitParms = limitParms;
   }
   
   public String getSearchString() {
      return this.searchString;
   }

   public void setSearchString(String searchString) {
      this.searchString = searchString;
   }

   public SearchMode getMode() {
      return this.mode;
   }

   public void setMode(SearchMode mode) {
      this.mode = mode;
   }

   public LimitParms getLimitParms() {
      return this.limitParms;
   }

   @Override
   public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);

   }
   
}
