package org.jbp.quote.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;
import org.jbp.quote.exception.NotFoundException;
import org.jbp.quote.model.Author;
import org.jbp.quote.model.AuthorSearchParms;
import org.jbp.quote.model.SearchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class MySqlAuthorDao implements AuthorDao {

   @Autowired
   private JdbcTemplate jdbcTemplate;
   @Autowired
   private NamedParameterJdbcTemplate namedParmJdbcTemplate;
   private static final Logger LOG = LoggerFactory.getLogger(MySqlAuthorDao.class);

   @Override
   public int create(Author author) {
      String sql = "INSERT INTO author (display_name) VALUES (:displayName)";

      KeyHolder keyHolder = new GeneratedKeyHolder();
      SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(author);
      this.namedParmJdbcTemplate.update(sql, parameterSource, keyHolder);

      return keyHolder.getKey().intValue();
   }

   @Override
   public void delete(int authorId) {
      String sql = "DELETE FROM author WHERE author_id = ?";

      int numRows = this.jdbcTemplate.update(sql, authorId);

      LOG.debug("{} rows deleted", numRows);
      if (numRows == 0) {
         throw new NotFoundException("author", Integer.toString(authorId));
      }
   }

   @Override
   public void update(Author author) {
      String sql = "UPDATE author SET display_name = :displayName WHERE author_id = :authorId";
      
      SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(author);
      int numRows = this.namedParmJdbcTemplate.update(sql, parameterSource);

      LOG.debug("{} rows updated", numRows);
      if (numRows == 0) {
         throw new NotFoundException("author", Integer.toString(author.getAuthorId()));
      }
     
   }

   @Override
   public Author findById(int authorId) {
      String sql = "SELECT author_id, display_name FROM author WHERE author_id = ?";
      
      List<Author> result = this.jdbcTemplate.query(sql,
               BeanPropertyRowMapper.newInstance(Author.class), authorId);

      if (result.size() == 1) {
         return result.get(0);
      }
      throw new NotFoundException("author", Integer.toString(authorId));
   }

   @Override
   public List<Author> findAll(@NonNull AuthorSearchParms parms) {
      final String sql =  "SELECT  author_id, display_name"
                           + " FROM author LIMIT :offset,:limit;";

      SqlParameterSource namedParameters = new MapSqlParameterSource()
               .addValue("offset", parms.getLimitParms().getOffset())
               .addValue("limit", parms.getLimitParms().getLimit());
      return this.namedParmJdbcTemplate.query(sql, namedParameters,
               (rs, rowNum) -> mapRow(rs, rowNum));
   }

   @Override
   public List<Author> findByName(@NonNull AuthorSearchParms parms) {

      // remove everything except characters, white space and ' and -
      // @see http://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html
      String searchParm = parms.getSearchString().replaceAll("[^a-zA-Z'\\-\\s]", "").trim();
      switch (parms.getMode()) {
         case EXACT_MATCH:
            searchParm = '"' + searchParm + '"';
            break;
         case MATCH_ALL:
            searchParm = addOperators(searchParm, s -> "+" + s);
            break;
         case WILDCARD:
            searchParm = addOperators(searchParm, s -> s + "*");
            break;
         case MATCH_ANY:
            // no formatting needed; just regularize the string
            searchParm = addOperators(searchParm, s -> s);
            break;
         default:
            throw new IllegalArgumentException("Unrecognized searchMode:" + parms.getMode());
      }
      LOG.debug("searchParm={}", searchParm);

      final String sql =
               "SELECT  author_id, display_name, MATCH(display_name) AGAINST(:name IN BOOLEAN MODE) AS score"
                        + " FROM author"
                        + " WHERE MATCH (display_name) AGAINST (:name IN BOOLEAN MODE)"
                        + " ORDER BY score DESC;";

      SqlParameterSource namedParameters = new MapSqlParameterSource("name", searchParm)
               .addValue("offset", parms.getLimitParms().getOffset())
               .addValue("limit", parms.getLimitParms().getLimit());
      
      return this.namedParmJdbcTemplate.query(sql, namedParameters,
               (rs, rowNum) -> mapRow(rs, rowNum));
   }

   private String addOperators(String searchParm, Function<String, String> mapper) {
      // split to words on white spaces or a dash, and add the operator according to the mapping
      // function, and return the individual words
      // separated by a single space
      return Stream.of(searchParm.split("\\s+|-")).map(mapper::apply)
               .collect(Collectors.joining(" "));
   }

   private Author mapRow(ResultSet rs, @SuppressWarnings("unused") int rowNum) throws SQLException {
      Author author = new Author();
      author.setAuthorId(rs.getInt("author_id"));
      author.setDisplayName(rs.getString("display_name"));
      return author;
   }

   @Override
   public List<Author> checkForDup(Author author) {
      AuthorSearchParms parms = new AuthorSearchParms(author.getDisplayName(), SearchMode.EXACT_MATCH);

      List<Author> authors = this.findByName(parms);
      authors.removeIf(a -> a.getAuthorId() == author.getAuthorId());
      return authors;
   }
}
