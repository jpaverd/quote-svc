package org.jbp.quote.data;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.jbp.quote.model.Author;
import org.jbp.quote.model.AuthorSearchParms;
import org.jbp.quote.model.SearchMode;

public interface AuthorDao {

	int create(Author author);
	void delete(int authorId);
	void update(Author author);

	Author findById(int authorId);
	/**
	 * @param name - List of words representing a name. Must contain at least one word. 
	 * 				Punctuation other than ' and - will be removed before searching
	 * @param mode - Defaults to MATCH_ANY if not provided
	 * @return list of authors matching the passed name using the search mode specified
	 * @see SearchMode
	 */
	List<Author> findByName(@NonNull AuthorSearchParms parms);
   
   List<Author> findAll(@NonNull AuthorSearchParms parms);
   List<Author> checkForDup(Author author);
}
