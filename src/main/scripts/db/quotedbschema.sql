-- -----------------------------------------------------
-- Schema quote
-- -----------------------------------------------------
-- DROP SCHEMA IF EXISTS `quote` ;

-- -----------------------------------------------------
-- Schema quote
-- -----------------------------------------------------
SHOW WARNINGS;

CREATE SCHEMA IF NOT EXISTS `quote` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `quote` ;

-- -----------------------------------------------------
-- Table `quote`.`author`
-- -----------------------------------------------------
-- DROP TABLE IF EXISTS `quote`.`author` ;

CREATE TABLE IF NOT EXISTS `quote`.`author` (
  `author_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `display_name` TEXT(128) NOT NULL COMMENT '',
  create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`author_id`)  COMMENT '',
  FULLTEXT `ix_display_name_text` (`display_name`)  COMMENT ''),
  UNIQUE INDEX `ix_display_name_uniq` ON author(`display_name` (128))
ENGINE = InnoDB;

-- DROP USER quoteapp;
-- CREATE USER 'quoteapp' IDENTIFIED BY 'quotepass';
-- GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE `quote`.* TO 'quoteapp';
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE `quote`.* to 'quoteapp'@'localhost' identified by 'quotepass';