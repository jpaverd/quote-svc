/**
 * 
 */
package org.jbp.quote.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.jbp.quote.QuoteApplication;
import org.jbp.quote.exception.NotFoundException;
import org.jbp.quote.model.Author;
import org.jbp.quote.model.AuthorSearchParms;
import org.jbp.quote.model.LimitParms;
import org.jbp.quote.model.SearchMode;
import org.jbp.quote.svc.Defaults;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * This is an integration test, because it connects to the quote database
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = QuoteApplication.class)
@Transactional // rollback after each test
public class AuthorDaoMySqlIT {

	private static final Logger LOG = LoggerFactory.getLogger(AuthorDaoMySqlIT.class);

	@Autowired
	private MySqlAuthorDao authorDao;

	private void findById(Integer id, String expectedName) {
		Author author = this.authorDao.findById(id);
		LOG.debug(author.toString());
		assertEquals(id, author.getAuthorId());
		assertEquals(expectedName, author.getDisplayName());
	}
	
	@Test
	public void testFindByIdFound() {
		findById(1, "Albert Einstein");
	}

	@Test
	public void testFindByIdForeign() {
		findById(10, "松尾 芭蕉 (Bashō)");
	}
	
	@Test(expected=NotFoundException.class)
	public void testFindByIdMissing() {
		this.authorDao.findById(-1);
	}

	@Test
	public void testDeleteFound() {
		int id = 1;
		this.authorDao.delete(id);
	}

	@Test(expected=NotFoundException.class)
	public void testDeleteMissing() {
		this.authorDao.delete(-1);
	}

	private void insert(String name) {
		Author authorIn = new Author();
		authorIn.setDisplayName(name);
		int id = this.authorDao.create(authorIn);
		LOG.debug("Added author with name '{}', id {}", name, id);
		
		Author authorOut = this.authorDao.findById(id);
		assertEquals(authorIn.getDisplayName(), authorOut.getDisplayName());
	}

	@Test
	public void testInsert() {
		insert("Bugs Bunny");
	}

	@Test
	public void testInsertForeign() {
		insert("孔夫子"); // Confucius
	}
	
	@Test
	public void testUpdate() {
		int id = 1;
		Author authorIn = this.authorDao.findById(id);
		LOG.debug("found author with name {}", authorIn.getDisplayName());
		final String newName = "Foo Bar";
		authorIn.setDisplayName(newName);
		this.authorDao.update(authorIn);

		Author authorOut = this.authorDao.findById(id);
		assertEquals(authorIn.getDisplayName(), authorOut.getDisplayName());
	}

   @Test(expected=NotFoundException.class)
   public void testUpdateMissing() {
      Author author = new Author(9999, "I don't exist");
      this.authorDao.update(author);
      fail("Update should have thrown an exception");
   }

   @Test
	public void testfindByNameWildcard() {
		String name = "John";
		AuthorSearchParms parms = new AuthorSearchParms(name, SearchMode.WILDCARD, new LimitParms());
		List<Author> authors = this.authorDao.findByName(parms);
		authors.forEach(a -> LOG.debug(a.toString()));
		assertEquals(2, authors.size());
	}

	@Test
	public void testfindByNameAnyAndWhitespace() {
		String name = "Douglas Adams";
      AuthorSearchParms parms = new AuthorSearchParms(name, SearchMode.MATCH_ANY, new LimitParms());
		List<Author> authors = this.authorDao.findByName(parms);
		authors.forEach(a -> LOG.debug(a.toString()));
		assertEquals(3, authors.size());
		// there are other matches, but the exact match should be first
		assertEquals("Douglas Adams", authors.get(0).getDisplayName());
	}

	@Test
	public void testfindByNameAll() {
		String name = " Douglas	 Adams  ";
      AuthorSearchParms parms = new AuthorSearchParms(name, SearchMode.MATCH_ALL, new LimitParms());
		List<Author> authors = this.authorDao.findByName(parms);
		authors.forEach(a -> LOG.debug(a.toString()));
		assertEquals(1, authors.size());
	}
	
	@Test
	public void testfindByNameExactNone() {
		String name = "John Kennedy";
      AuthorSearchParms parms = new AuthorSearchParms(name, SearchMode.EXACT_MATCH, new LimitParms());
      List<Author> authors = this.authorDao.findByName(parms);
		authors.forEach(a -> LOG.debug(a.toString()));
		assertEquals(0, authors.size());
	}

	@Test
	public void testfindByNameExact() {
		String name = "John F Kennedy";
      AuthorSearchParms parms = new AuthorSearchParms(name, SearchMode.EXACT_MATCH, new LimitParms());
		List<Author> authors = this.authorDao.findByName(parms);
		authors.forEach(a -> LOG.debug(a.toString()));
		assertEquals(1, authors.size());
	}

	@Test
	public void testfindByNameApostrophe() {
		String name = "O'Brien";
      AuthorSearchParms parms = new AuthorSearchParms(name, SearchMode.MATCH_ANY, new LimitParms());
		List<Author> authors = this.authorDao.findByName(parms);
		authors.forEach(a -> LOG.debug(a.toString()));
		assertEquals(1, authors.size());
	}
	
	// @TODO implement code and test offset limit for find by name

	@Test
	public void testfindByNameDash() {
		String name = "Least-Heat";
      AuthorSearchParms parms = new AuthorSearchParms(name, SearchMode.MATCH_ALL, new LimitParms());
		List<Author> authors = this.authorDao.findByName(parms);
		authors.forEach(a -> LOG.debug(a.toString()));
		assertEquals(1, authors.size());
	}

	@Test
   public void testfindAllDefault() {
      List<Author> authors = this.authorDao.findAll(new AuthorSearchParms());
      assertEquals(Defaults.DEFAULT_LIMIT, authors.size());
   }

	@Test
   public void testfindAllOffsetLimit() {
	   LimitParms limitParms = new LimitParms();
      int resultCount = 5;
	   limitParms.setLimit(resultCount);
      limitParms.setOffset(2);
      AuthorSearchParms parms = new AuthorSearchParms(null, null, limitParms);
      List<Author> authors = this.authorDao.findAll(parms);      assertEquals(resultCount, authors.size());
      Author author = authors.get(0);
      assertEquals(3, author.getAuthorId().intValue());
   }
}
