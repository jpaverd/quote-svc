package org.jbp.quote.svc;

import static org.junit.Assert.fail;

import java.util.List;

import org.jbp.quote.data.AuthorDao;
import org.jbp.quote.exception.BadRequestException;
import org.jbp.quote.model.Author;
import org.jbp.quote.model.AuthorSearchParms;
import org.jbp.quote.model.SearchMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DefaultAuthorSvcTest {

   @Mock
   private AuthorDao authorDao;

   @InjectMocks
   private DefaultAuthorSvc authorSvc;

   @Test(expected=BadRequestException.class)
   public void testUpdateErrors() {
      Author author = new Author();
      this.authorSvc.create(author);
      fail("Should have thrown exception");
   }

   @Test(expected=BadRequestException.class)
   public void testFindBadParms() {
      AuthorSearchParms parms = new AuthorSearchParms();
      parms.setMode(SearchMode.EXACT_MATCH);
      @SuppressWarnings("unused")
      List<Author> authors = this.authorSvc.find(parms);
   }

}
