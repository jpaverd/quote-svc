/**
 * 
 */
package org.jbp.quote.svc;

import org.jbp.quote.QuoteApplication;
import org.jbp.quote.exception.DuplicateException;
import org.jbp.quote.model.Author;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * This is an integration test, because it connects to the quote database
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = QuoteApplication.class)
@Transactional // rollback after each test
public class DefaultAuthorSvcIT {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultAuthorSvcIT.class);

	@Autowired
	private AuthorSvc authorSvc;

	@Test(expected=DuplicateException.class)
	public void testCreateDup() {
	   Author author = new Author("Albert Einstein");
		this.authorSvc.create(author);
	}

   @Test
   public void testCreateSuccess() {
      Author author = new Author("Carlos Santana");
      int newId = this.authorSvc.create(author);
      LOG.debug("id={}",newId);
      Assert.assertTrue(newId > 0);
   }
   
}
