package org.jbp.quote.rest;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtil {

   private TestUtil() {}
   
   public static String json(Object obj) throws IOException {
      ObjectMapper mapper = new ObjectMapper();
      return mapper.writeValueAsString(obj);
   }
}