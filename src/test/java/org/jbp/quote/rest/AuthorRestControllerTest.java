package org.jbp.quote.rest;

import java.nio.charset.Charset;

import org.hamcrest.Matchers;
import org.jbp.quote.exception.NotFoundException;
import org.jbp.quote.model.Author;
import org.jbp.quote.rest.controller.AuthorRestController;
import org.jbp.quote.rest.controller.GlobalDefaultExceptionHandler;
import org.jbp.quote.svc.AuthorSvc;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * @see "http://stackoverflow.com/questions/21216349/unit-testing-spring-mvc-controller-with-mockito"
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorRestControllerTest {

   @Mock
   private AuthorSvc authorSvc;

   @InjectMocks
   private AuthorRestController authorController;

   private volatile MockMvc mockMvc;

   private static final MediaType CONTENT_TYPE = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

   private GlobalDefaultExceptionHandler excpHndlr = new GlobalDefaultExceptionHandler();

   @Before
   public void setup() {
      // MockitoAnnotations.initMocks(this); // @RunWith(MockitoJUnitRunner.class) does this automatically
      this.mockMvc = MockMvcBuilders.standaloneSetup(this.authorController)
               .setControllerAdvice(this.excpHndlr).build();
   }

   @Test
   public void testgetAuthor() throws Exception {
      final int id = 1;
      final String name = "Anon Y Mous";
      final Author author = new Author(id, name);
      Mockito.when(this.authorSvc.findById(id)).thenReturn(author);

      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors/" + id))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.jsonPath("$.authorId", Matchers.is(id)))
               .andExpect(MockMvcResultMatchers.jsonPath("$.displayName", Matchers.is(name)));
   }

   @Test
   public void testgetAuthorNotFound() throws Exception {
      final int id = 99;
      final String idString = Integer.toString(id);
      Mockito.when(this.authorSvc.findById(id))
               .thenThrow(new NotFoundException("Author", idString));

      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors/" + id))
               .andExpect(MockMvcResultMatchers.status().isNotFound())
               .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.jsonPath("$.message",
                        Matchers.is("There is no Author with id '" + idString + "'")));
   }

   @Test
   public void testFindAuthorsInvalidParm() throws Exception {
      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors?mode=x&offset=y&limit=z"))
      .andExpect(MockMvcResultMatchers.status().isBadRequest());
   }

   @Test
   public void testCreateAuthor() throws Exception {
      int id = 23;
      Mockito.when(this.authorSvc.create(org.mockito.Matchers.any())).thenReturn(id);
      
      Author author = new Author(null, "Anon Y Mouse");
      String json = TestUtil.json(author);
      
      this.mockMvc.perform(MockMvcRequestBuilders.post("/authors")
               .content(json)
               .contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.status().isCreated())
               .andExpect(MockMvcResultMatchers.header().string("Location", Matchers.endsWith(Integer.toString(id))));
   }

   
   @Test
   public void testUpdateAuthor() throws Exception {
      int id = 2;
      Mockito.doNothing().when(this.authorSvc).update(org.mockito.Matchers.any());
      
      Author author = new Author(null, "Anon Y Mouse");
      String json = TestUtil.json(author);
      
      this.mockMvc.perform(MockMvcRequestBuilders.put("/authors/" + id)
               .content(json)
               .contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.status().isNoContent());
   }

   
   @Test
   public void testDeleteAuthor() throws Exception {
      int id = 2;
      Mockito.doNothing().when(this.authorSvc).delete(id);
      
      this.mockMvc.perform(MockMvcRequestBuilders.delete("/authors/" + id)
               .contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.status().isNoContent());
   }

}
