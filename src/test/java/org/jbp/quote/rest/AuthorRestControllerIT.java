package org.jbp.quote.rest;

import java.nio.charset.Charset;

import org.hamcrest.Matchers;
import org.jbp.quote.QuoteApplication;
import org.jbp.quote.data.MySqlAuthorDao;
import org.jbp.quote.model.Author;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = QuoteApplication.class)
@WebIntegrationTest({"server.port=0"}) // run test on a random port
@Transactional
public class AuthorRestControllerIT {

   private static final Logger LOG = LoggerFactory.getLogger(MySqlAuthorDao.class);

   @Autowired
   private volatile WebApplicationContext webApplicationContext;

   private volatile MockMvc mockMvc;

   private static final MediaType CONTENT_TYPE = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

   @Before
   public void setup() throws Exception {
      this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
   }

   @Test
   public void testGetAuthor() throws Exception {
      int id = 1;
      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors/" + id))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.jsonPath("$.authorId", Matchers.is(id)))
               .andExpect(MockMvcResultMatchers.jsonPath("$.displayName", Matchers.is("Albert Einstein")));
   }

   @Test
   public void testGetAuthorsDefault() throws Exception {
      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors"))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(10)))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].authorId", Matchers.is(1)));
   }

   @Test
   public void testGetAuthorsOffSetLimit() throws Exception {
      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors?limit=3&offset=2"))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(3)))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].authorId", Matchers.is(3)));
   }

   @Test
   public void testGetAuthorsNoResults() throws Exception {
      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors?q=Bobo the Clown"))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(0)));
   }

   @Test
   public void testGetAuthorsModeSearch() throws Exception {
      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors?q=Douglas Adams&mode=MATCH_ANY"))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(3)))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].authorId", Matchers.is(3)));
   }


   @Test
   public void testAddUpdateDelete() throws Exception {

      Author author = new Author(null, "Anon Y Mouse");
      String json = TestUtil.json(author);
      MvcResult mvcResult;

      mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post("/authors")
               .content(json)
               .contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.status().isCreated())
               .andReturn();

      String location = mvcResult.getResponse().getHeader("Location");
      int id = Integer.parseInt(location.substring(location.lastIndexOf('/') + 1));
      LOG.debug("added, id={}", id);

      String newName = "Another Mouse";
      author.setDisplayName(newName);
      json = TestUtil.json(author);
      this.mockMvc.perform(MockMvcRequestBuilders.put("/authors/" + id)
               .content(json)
               .contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.status().isNoContent());

      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors/" + id))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.jsonPath("$.authorId", Matchers.is(id)))
               .andExpect(MockMvcResultMatchers.jsonPath("$.displayName", Matchers.is(newName)));

      this.mockMvc.perform(MockMvcRequestBuilders.delete("/authors/" + id)
               .contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.status().isNoContent());

      this.mockMvc.perform(MockMvcRequestBuilders.get("/authors/" + id))
               .andExpect(MockMvcResultMatchers.status().isNotFound());

   }

   @Test
   public void testUpdateDup() throws Exception {
      // change Albert Einstein's name to a name already assigned to another author
      Author author = new Author();
      author.setDisplayName("Henry Ford");
      this.mockMvc.perform(MockMvcRequestBuilders.put("/authors/1")
               .content(TestUtil.json(author))
               .contentType(CONTENT_TYPE))
               .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());
   }
}
