# quote-svc

quote-svc is a RESTful service to maintain a database of quotations. This application was created as an exercise to learn RESTful architecture and Spring Boot.

## Dependencies

### Build
* Java 8 JDK
* Maven 3
* Internet access (for Maven Dependencies)

### Execution
* Java 8 JRE
* MySQL 5.6

## Setup

1. Start MySQL
2. Create the quote database: `mysql -uroot -p  <src/main/scripts/db/quotedbschema.sql`
3. (optional) load sample data (required for the integration tests): `mysql -uroot -p  -e "source src/main/scripts/db/quotedbdata.sql` quote
4. To build the application, run `mvn clean package` from the project root directory.
5. (optional) Run `mvn verify` to run the integration tests.

## Execution

### Starting quote-svc
1. Start MySQL.
2. Start the service: from the project root, `mvn spring-boot:run` (or `cd ./target; java -jar quote-svc-<version>.jar`) 

### Stopping quote-svc
1. POST http://localhost:8080/shutdown HTTP/1.1
2. (optional) Stop MySQL

## Usage

The authors resource is at http://localhost:8080/authors. There is a soapui project at docs/soapui that contains example requests.

There is also a Swagger specification at doc/api-docs.json

## History

This is the initial version.

## Contributors

* John Paverd

## How to Contribute

[Bitbucket Forking Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

## License

This is open-source software released under [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/)
